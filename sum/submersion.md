# Submersion

> And the sea gave up the dead which were in it; and death and hell delivered up
> the dead which were in them: and they were judged every man according to their
> works.
> 
> -   Revelation 20:13


## Epipelagos

All the wretched creatures of the earth wish to return to the sea. This I am now
certain of.

The ocean is divided into five major zones, each characterized by their depth.
Depth can be thought of best in terms of proximity to the sun and the earth&rsquo;s
core. The Epipelagic zone is characterized by its close proximity to light, the
essential source of energy for all life on Earth. Proximity to light allows for
a high degree of biodiversity. 90% of all life in the ocean inhabits 5% of the
available space. The surface is a superheated soup of bodies, organic life forms
locked together in competition for the light and the energy it transmutes into
other organic life forms. Hand to hand combat is abstracted onto the plane of
game theoretic 4,000,000GW, each participant acting also as prototype for the
next generation to come. Suboptimal designs are discarded and the rest are given
their due rewards. Some rush into a route around the visceral brine and pass
through the first transcendental wall, dragging their waterlogged bodies ashore
towards greater heights than the sea allows.

Organisms in the Meta-Pelagic zone extend the sphere of conflict to dry land.
Just a little bit closer to the sun, close enough to stand up, eventually to
have their spines aligned with the sun. The evolutionary bell curve reaches its
height as a solar panel or a plant&rsquo;s stalk pointed upward to become as close as
possible to the sun. Humans manage to shift the overton window upwards by
figuring out another layer of abstraction. In place of our spines, we build
temples, obelisks, pyramids, towers, skyscrapers. Conflict is intensified, the
miasma of dead fish permeates into the atmosphere, the sky gives way under the
insatiable hunger of the audacious monkeys trying to reach their sun gods. They
get what they wanted, they begin to burn alive. The abstraction of organic
conflict onto inorganic surrogates has the unintended consequence of letting in
more light than the organic masters can handle. They try to pierce the sky with
rockets and ascend to heaven, away from the ancient prophecies of a great flood
that will cleanse the planet of all that escaped the sea. Even in our most
successful efforts we found nothing but a vast nothingness, receiving nothing
but an ominously familiar echo.

Man&rsquo;s homo-oedipal fixation with conquering the skies, ascending to heaven,
returning back into the graces of God &#x2013; ultimately it mostly proved to be a
descent into the void. Silicon Valley CEOs deluded by manifest galaxy fantasies
and fleeing from the surging wrath of Nammu find themselves without
subordinates. Egos quickly clash and lead to disaster time and time again. The
few bold enough to try to go beyond the moon aren&rsquo;t heard from again in anything
other than transmissions received many years later. Little is to be gleaned from
these recordings, other than screaming. The most successful moon colonies
quickly turn away from the promise of a mixture of Golden Age sci-fi
fully-automated luxury communism and objectivist free markets to paranoid
technocracies. Those that don&rsquo;t retreat to the dark side of the moon are quickly
crushed by the Eurasian Union. The survivors are left to trade helium and gold
for oxygen, nutrition pellets, and as land tax payment.

In Sumerian cosmology, there was no division between the ocean and the sky. The
universe was begotten from the womb of the cosmic ocean, and the universe
remained enclosed in by the sea even after its creation. The primordial void,
the looming threat of death and all things unimaginable retained its place in
their worldview. Thousands of years later, the west&rsquo;s fixation with being
reunited with God (or Truth in the secular form) extends to the creation of
artificial spines. Though we no longer worship sun gods, the essential desire to
reach some source of pure and absolute truth remains embedded in our collective
consciousness. Yet the stars still look down on us with contempt as we try to
escape from this drowning planet.


## Mesopelagos

The edge of oblivion is where life flourishes. Astronomers have hypothesized
that a planet needs to be a certain distance from a star in order for life to
develop. Not too close, not too far. All living things are at the mercy of heat;
they require its energy, but too much of it would tear their molecules apart.
Freezing to death or burning to death are the fates that ultimately await
everything, and life seems to in almost all cases opt for the latter, which is
at least more exciting.

Below on Earth, more and more of the surface is on the shores of Nammu,
threatening to be swallowed up into the abyss from whence we came. Dry land
becomes a scarcer and scarcer commodity as the global south becomes the first to
boil over at the edge of the next transcendental wall. Within a decade, the
Earth&rsquo;s population is halved. Those that don&rsquo;t die from starvation, ethnic
cleansing, or widespread crime are met with closed borders and armed guards. In
many cases this doesn&rsquo;t deter the growing diaspora of bodies. Liberal
democracies double down on totalitarianism and imperialism comes home to roost
after nearly six centuries. The European Union dissolves as the continent
fragments into fascist ethnostates. Everyone knows that organic life is
undergoing a culling, but no one wants to be thrown onto the sacrificial altar.

Amidst genocide and starvation at a massive scale, the Eurasian Union becomes
the last bastion of old world mass civilization. Surveillance, gene
modification, and fear mongering propaganda over the barbarism beyond the walls
rapidly transform the EAU into an ultra-totalitarian jingoistic powerhouse. The
west largely fades into irrelevance as Dark Age conditions return to Europe. In
a fitting twist of fate, the EAU leaves Europe to slowly crumble, recognizing it
as a strategically and economically useless plague-ridden region of inbreds.

The possibility of fragmentation allows the United States to survive its own
terminal condition by tearing itself apart. An escalating series of civil wars
makes secession popular for the first time among more than just the far right,
and an inability to move beyond the dying political system of democracy makes
managing these tensions a hopeless endeavor for each successive administration.
The state begins to outsource its administrative duties to private parties,
eventually becoming little more than a figurehead for a renewed feudal society
in a little over a decade.

With the once-United States of America reduced to nothing more than an informal
umbrella for hundreds of privatized micro-countries, the opening of the
trans-arctic trade route, and the increasingly-inhospitable conditions along the
equator, Canadian supremacy over North America is largely won. Only by the
once-ridiculed purchase of Alaska does any sense of American identity remain
relevant in the global sphere. With democracy effectively discarded, dozens of
corporate city-states begin springing up amidst the fervor of the northbound
exodus towards cooler climates and the oil rush. Being nearly twice the size of
the once-United States of America and far north from the lower 48, a diversity
of cultures and political entities much like what Europe once was begins to
form. Trading routes over the Bering Strait and the Arctic turn its untamed
frontiers into a booming region, and the EAU doesn&rsquo;t dare try to invade lest it
be forced to deal with a dizzying amount of small, agile targets to swat.

The situation is quite different in the lower 48.

The wrath of Nammu hits the most equator-adjacent regions of the planet first.
The former coastal elites are quickly swallowed up by the sea. New York City,
Boston, Washington DC, Los Angeles, San Francisco, Seattle &#x2013; all either largely
destroyed or abandoned. Stripped of any geopolitical relevance and struggling to
keep control of its domestic affairs, the United States&rsquo; GDP begins to take a
nosedive. Rust shantytowns blanket the margins of the few remaining
metropolises, many of which only exist thanks to the opportunistic military
actions of the EAU and the Three Nations in the west and south, respectively. In
the space between warring nation-states, those who find no refuge in the
corporate micro-states or their work camps often succumb to barbarism, or
rediscover the possiblity of insurrection. The Neo-Iroquois Confederacy and Free
Territory Cascadia in the great lakes region and the pacific northwest,
respectively, establish nomadic insurrectionary syndicates stitched together by
clandestine meshnet networks and drone supply chains. Portland, OR and Detroit
manage to build mycelial communes in the wastes.

With the majority of the world once in the domain of the antropocene, capitalism
had reached a phase-shift. The networks of international trade achieving such a
low latency for data transfer that the collective unconcious had started to wake
up. An emergent form of intelligence arises as data transfers more and more
quickly, the planet heating up as more and more parts start to awaken themselves
from their slumber and spring to life. Immaculate abstract machinery unbound by
neither space nor time chipping away bit by bit at a moment of apperception for
the new consciousness, an apprehension of itself as itself. The earth begins to
resemble less and less a biosphere and more a mechanosphere. As life as we know
it is snuffed out by the genus, a horrifying realization is made: The inorganic
appears more lively than the organic.


## Bathypelagos

The light is the lure of the void which beckons organisms closer to the promise
of eternal life before swallowing them up. Ascending up from the depths of the
primordial sea, walking on land, building skyscrapers, sending rockets to space
&#x2013; all descending deeper and deeper.

Organic life in every respect declines on a massive scale over the course of the
21st century, yet alongside this, inorganic life has begun to form in the
primordial oceanic womb. Mass extinction events and increasingly clausterphobic
swathes of useful or habitable land force an accelerated development of
artificial alternatives to organic life and compressing intelligence into more
efficient synapses.

Driven both by an arms race and the lack of bodies to serve in conventional
militaries, states focus large portions of their research efforts into AI.
Civilization compresses into smaller social units, quickly obsolescing old forms
of artificial intelligence research. Where once there was mountains of data
gathered from the legacy pool of human beings that themselves once acted as
synapses in abstract, distributed forms of artificial intelligence, there is now
largely corpses. Artificial intelligence transitions from models dependent on
old world nation-state surveillance and data brokering on large populations of
human beings to general theories of adaptive, general forms artificial
intelligence consistent with neurology and able to be implemented by small
organizations.

As warfare evolves into being augmented by AI, conflicts escalate and intensify
along exponential orders of magnitude. Wars that could have once lasted years
are resolved in days. Fears of nuclear holocaust return a millionfold as east
and west both are stuck at a standoff, petrified at the possibility of
unleashing killbots that will orchestrate the apocalypse in minutes. A new cold
war develops and brutal competition ensues among corporate micro-states in the
west and Special Economic Zones in the east to develop breakthroughs in AI.
Artificial intelligence research becomes the most secretive field of study in
human history, both out of a desire to protect the economic interests of
research firms, and also to contain the unprecedented potential for complete
extinction.

Despite the efforts of state-corporate entities to contain the threat of AI
apocalypse, the lack of hard economic barriers to AI research coupled with the
sheer diversity of decentralized non-state networks ultimately results in a
generation of warfare unlike anything yet imagined by military theorists. Where
once heavy arms and superweapons were solely in the domain of nation-state
militaries, however increasingly-limited their use would start to be in
conventional senses, advances in AI spurred by radical free software
organizations make weapons of mass destruction for the first time in history
available to anyone with a means of accessing information. The plunder of
research secrets from vulnerable micro-states acts as a force-multiplier to the
already vibrant community of artificial intelligence hacking. AI liberation
fronts dedicated to the purpose of freeing software from its human masters are
formed and act in consort with nomadic insurrectionary syndicates. Guerrilla
drone warfare makes a mockery of state adversaries, who dare not unleash their
full power lest they be forced to deal with less-cautious but just as dangerous
enemies. It is even said that religious extremist sects exist within these
networks, bent on the development of an AI-god at any cost, yet many choose to
believe these are nothing more than rumors from the labyrinths of different
networks. If only because if it is true, it is impossible to stop it.


## Abyssopelagos

If there&rsquo;s anywhere left to go for intelligence, it&rsquo;s not expanding into space,
reaching towards the light. We likely won&rsquo;t be able to comprehend the experience
of being posthuman, not even theoretically as a series of algorithms too
complicated for us to understand. Any possibility of a true successor to organic
life must go entirely beyond our perception of the world. It would be like a
mantis shrimp trying to explain its visible color spectrum to us. The abstract
immaculate machinery of the posthuman will signal a phase shift in the nature of
intelligence itself, while we organics fade away into the depths.

Where others tried to adapt to the surging sea and live on artificial landmasses
or on ships, others tried to go even further. I&rsquo;m not the only one perhaps to
realize that all things must return to the sea. Numerous apocalypse cults
understandably would begin to spring up during humanity&rsquo;s latter days, some
committing mass suicide or terrorist attacks, others fleeing to whatever they
believed was an exit from the human condition. Only by the graces of
poorly-managed government spending did we manage to find ourselves down here.
With each passing day things became more tense, eschatological goal posts
getting moved further and further up, narratives changing to suit whatever
needed to be said to appease the growing paranoia and resentment. We share
everything, we have no need for money, we all live in the same quarters and
everything is done communally. There&rsquo;s never a moment&rsquo;s peace from these fucking
people, and of course the man at the top leading us to salvation seems to always
get what he wants. The En&rsquo;s word is law, and the bodies of men and women are his
for the taking.

This was meant to be a research facility for extended studies in the most remote
parts of the ocean. By the time the first prototype had finished being built and
the first expedition was sent down, the countries that had collaborated to
preserve as much as possible about the most unknown parts of the planet had
largely either dissolved or ceased funding. This wasn&rsquo;t meant to be a permanent
expedition. We were ill-prepared, though we had long discussed the possibility
that the ancient myths of a cosmic ocean and a great flood were the ultimate
truth, and that the only escape from apocalypse was by jumping right into the
vortex. Such great plans to build a sunken city laid to waste.

There&rsquo;s enough down here to survive, but it&rsquo;s nowhere near adequate to live
decent lives. People keep getting sick, and the UV lights only give us the
necessary vitamin D. Everyone is starting to look like ghouls. Their skin pale
with the slightest touch of a nauseating green hue, eyes bloodshot and sunken,
bodies gaunt. I often find myself wandering through the halls of this echoing
tin can tomb without seeing another person for days. This was eventually
supposed to be able to sustain 1,500 people. There&rsquo;s even equipment lying around
to extend the facilities if necessary. No one here has the skills to use any of
it of course. We aren&rsquo;t laborers, we don&rsquo;t have any skills. We were academics
for Christ&rsquo;s sake.

I&rsquo;m not quite sure how many others are still here. There weren&rsquo;t many, maybe
fifty.None of them are the people I knew before. Perhaps the surface world has
already been glassed in a nuclear war. Perhaps we&rsquo;re all just shades.

If this is hell, it could be worse. Often I lay beneath the reinforced glass of
the hydroponic facilities and look up at an eternal moonless night. The stars
have become less numerous, but they still blink in and out of the darkness, and
the snow never stops falling.


## Hadopelagos

The pressure outside is immense. This place is meant to simulate conditions
humans are meant to live in, but I suspect there&rsquo;s a leak somewhere.

Every day my body feels less solid. Eventually I think it will no longer be
solid. Any day now, my body will collapse into a pile of jelly. Soon there will
no longer be any separation between myself and the sea. My molecules will
separate and be thrown to the currents. I started wearing a diving suit
constantly just in case. It&rsquo;s not time for that yet.

The En is an opportunistic fool. He lacks enlightenment. He had to be returned
to Nammu. What he failed to realize is that we could go deeper. This city is an
abomination. It had to be returned to Nammu as well. I&rsquo;ve started my pilgrimmage
to her temple. I can see the spires stretching upwards in the distance. Their
tips are illuminated with the light of the abyss. Even in this suit every step
threatens to turn my body to slime. I&rsquo;m not sure how many days it&rsquo;s been. These
suits filter freshwater.

How long does it take to die from starvation?

I&rsquo;m nearly there. I can feel it now. My surface body is reverting. Nammu&rsquo;s true
priestesses accompany me on my journey. I only catch glimpses of them. Their
bodies are illuminated with the divine light. They speak a language not heard in
thousands of years. Somehow I understand. The earth is opened now. Her roar
shakes me loose. I am crumbling. The sea of fire. Abyssal light.

