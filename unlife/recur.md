# Recurrence

It&rsquo;s been over a year since I&rsquo;ve posted anything, and about two years since I&rsquo;ve
posted anything that hasn&rsquo;t been, well, whatever the fuck that was. Am I going
to elaborate on anything? No, fuck you, here&rsquo;s a meta post.<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>

I&rsquo;ve been meaning to migrate my site away from Hugo for awhile now. I find its
templating system to be a pain in the ass to use if you want to customize your
site and have some kind of idea what you&rsquo;re doing, and more recently I&rsquo;ve
realized that Hugo is actually quite difficult to build from source after a
brief stint using [GNU Guix](https://guix.gnu.org/) and attempting to package some stuff. While I applaud
the efforts of Guix and NixOS for trying to bring some form of order and sanity
to the designated shitting street of Unix-like OSes that is GNU/Linux,
ultimately I&rsquo;ve come to the realization that the very need for such additional
layers of complexity tells me that there&rsquo;s something wrong with the foundations
of the software. But that&rsquo;s a topic for a different post.

Suffice to say that I decided to stop using Hugo because if I can&rsquo;t fit
realistically fit all of its code and the code it depends on in my head, I
shouldn&rsquo;t be using it in the first place. It&rsquo;s only a fucking static site
generator, after all. Why something that does something so simple needs to be so
complicated is something I simply can&rsquo;t understand.

But this was only compounded upon by the fact that the blogging package I was
using for emacs, ox-hugo, just stopped working. Again, more complexity that I
shouldn&rsquo;t need that ends up biting me in the ass and ruins my whole blogging
workflow.

Until I finish working on my own static site generator thing in common lisp,
the blog will exist in this current form with me basically editing all of the
HTML by hand aside from exporting these posts from org-mode using the built-in
exporter for emacs. Gnon help me, I already hate this.

I am also switching to this new domain because it&rsquo;s vastly superior to the old
one. I remember once having a conversation with lilli patch (aka lilypatchwork)
about .xyz domains being the official TLD for Neo-Amazonia or Gack Girls or
whatever, but truthfully I just picked that domain whenever I got it (2017?
fucking hell) because I couldn&rsquo;t think of anything better. I&rsquo;ve always kinda
hated my old domain name.

I don&rsquo;t know how to end this post. Does anyone even still give a fuck what I do
or say? I&rsquo;m gay and retarded. I&rsquo;m damaged and deranged. I&rsquo;m boiling in oil. I&rsquo;ll
post something of more substance soon.


## Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> I will
elaborate in a future post on some stuff.
