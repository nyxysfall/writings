# Should You Use the Fediverse? A Disinterested Luser’s Take

I&rsquo;ve meant for awhile now since I first made a fediverse account (back in 2017,
when Mastodon first started getting a lot of attention) to write an analysis
about it, but in light of recent events with Elon &ldquo;The Big Chungus&rdquo; Musk
acquiring Twitter and a potential resurgence of people leaving Twitter it felt
appropriate to write a brief take on it for people who may be interested in
joining. Partly this is because I happen to run an instance and am writing this
for an audience of people who I am kind of sort of <del>shilling</del> trying to convince
to join it, but this is a &ldquo;disinterested luser&rsquo;s take&rdquo; on fedi because really I
don&rsquo;t care that much aside from my own personal interest in having more of a
diverse culture on fedi. If you don&rsquo;t use xenofem.me, whatever, enjoy getting
your IV drip of performative outrage on Twitter. Also, this isn&rsquo;t yet another
explanation of what the fediverse is; there are plenty of other guides out there
for that. It also isn&rsquo;t an appeal to using free software or having &ldquo;privacy&rdquo;
with your data, because those things rarely ever convince anyone to use a
network. The only way to get anyone to even consider using something like fedi
is by talking about the kinds of people on it, because people are the product on
social media; the software is incidental to this, even if the product isn&rsquo;t
being monetized by anyone.

So to get right into it: To me the main problem with fedi by far, that I&rsquo;ve
talked about ad nauseaum at this point elsewhere, is that there are basically
three major groups of people on fedi. There are people who are mostly there
because they&rsquo;re free software hippies who only talk about touching computers,
and then there are far right and &ldquo;far left&rdquo; people who were forced into seeking
out alternatives to mainstream social media because they keep getting banned for
posting racial slurs or whatever it is that the radikweers get banned for. If
you don&rsquo;t fall into the first group and aren&rsquo;t part of the other two groups, by
which I mean that you basically only ever do ingroup signaling about having the
same shared culture and politics, you will probably hate fedi. But that might
also be why you should join.

My personal experience as a fedi user has been sometimes rewarding because it&rsquo;s
lead to me pursuing tech niches (BSD and Common Lisp) that I may not have
otherwise explored (or at least not gotten to until later) without being in an
environment where discussions around those sorts of things can happen. I&rsquo;m a
pretty solitary person for the most part, but being in an environment where
people have some shared interests does expedite the process of finding a niche
in a particular interest and learning more about it, which was originally how I
got into accelerationism via #RhettTwitter and #CaveTwitter back in 2017-18. The
rest of the time has been less so, because I don&rsquo;t really want to spend all of
my time talking about and thinking about computer shit (even though I at least
have a genuine appreciation for Common Lisp and programming in it).

One major problem with fedi if you don&rsquo;t fall into the dominant three cultures
is that unlike other social media networks/platforms that people are already all
on anyways, you are limited in what you can talk about without either shouting
into a void where you don&rsquo;t even see stuff on your feed that is relevant to your
interests, or worse you risk being dogpiled by the people who are already there
and are afraid of their community being Eternal Septembered.<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> I constantly see people making annoying saccharine comments about
how fedi is such a heckin wholesome and close-knit community of people that
isn&rsquo;t poisoned by the performative outrage machine of engagement-driven social
media platforms that use algorithms to manipulate the behavior of the users.
Fedi software shows you your feed chronologically and doesn&rsquo;t have algorithmic
content curation, because there aren&rsquo;t really any server instances that are
funded by ads and would benefit materially from implementing a way of
manipulating user behavior for engagement.<sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup> But while this might be the case, my experiences thus far
have been that social media companies aren&rsquo;t some evil alien force that has
changed human behavior in some fundamentally bad way; they are, rather, exactly
what we as a species deserve. People like to talk about &ldquo;cancel culture&rdquo;,
culture wars, people being extremely politically divided these days, but it&rsquo;s
not as if we didn&rsquo;t already have terms like &ldquo;witch hunts&rdquo; referring to phenomena
that have existed for literal hundreds of years, and it&rsquo;s not as if Marshall
McLuhan&rsquo;s idea of a global village wasn&rsquo;t developed long before the Internet was
a widely used network. I&rsquo;ve started to think that social media is more of an
evolution of fundamentally bad aspects of human psychology when it scales out.
An optimized version of it where we now have a way of quantifying things that
have always been there.

And while I&rsquo;m on that topic, I should also add that another thing I really
despise about fedi is that pretty much all the major server softwares for it
(Pleroma, Mastodon, Misskey) are just clones of Twitter with a few bells and
whistles but that otherwise do nothing innovative or new. They were born out of
the same mindset that lead to the creation of GNU Social before it and GNU/Linux
before that of trying to compete with something that is fundamentally bad
(Twitter in the former case, Microsoft Windows in the latter ultimately with how
Linux distros have designed their overall UX to be familiar to end users coming
from Windows and MacOS). In all these cases the goal has never been to rip out
the roots of something fundamentally bad and create something new and innovative
that could condition people&rsquo;s desires to be something other than the endless
repetition of the same, but rather to create a harm reduction alternative to it.
That&rsquo;s how I&rsquo;ve come to view fedi in a lot of ways, as a methadone for social
media addicts. If you&rsquo;re looking for a less overtly terrible version of social
media but aren&rsquo;t ready to pull the plug entirely and do something useful with
your time, consider joining fedi. Be warned though that because fedi is a small
network mainly comprised of people who are too misbehaved to last on Twitter
(which includes myself I guess), most of them are going to be assholes who will
dogpile you if you talk about something they don&rsquo;t like. Which means that if
you&rsquo;re a trans person for instance, there&rsquo;s a high likelihood that you&rsquo;ll get
dogpiled by the reactionaries and the radikweers for posting something that fits
in that sweet spot of being unwoke but also still related to queer/trans issues.
If you avoid talking about political shit or software, and don&rsquo;t post the type
of content that appeals to people who are 4chan refugees or Twitter/Tumblr
refugees who want to see a certain brand of culture represented, you&rsquo;ll probably
just be talking to yourself.

But paradoxically this is why I think it&rsquo;s probably better to join fedi instead
of staying on Twitter, notwithstanding the many problems Twitter has, if you&rsquo;re
interested at least in being able to potentially have more rewarding
interactions with people. I hope to see fedi&rsquo;s duoculture<sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>
end eventually, because I would like to have a space to talk about my various
interests outside of tech where I can do more than talk to myself and where if I
do make a bunch of people mad for posting something I will at least not feel
completely isolated. And I think it&rsquo;s time better spent investing your time and
effort into creating a space for good discussions and interactions to happen
where you actually can have some autonomy and investment in it, instead of being
at the whims of algorithms that will likely one day short-circuit and ban your
account for nothing, or of Silicon Valley ad companies pretending to be tech
companies. What I&rsquo;ve not mentioned here yet is that the main reason I decided to
make a fedi account again last year by making my own instance was because after
having two of my accounts banned from Twitter for completely bullshit reasons, I
realized that giving away my content to a company that sells it to advertisers
and can and will ban me for nothing while also retaining my content forever to
continue selling to advertisers is incredibly stupid and pathetic. It might just
be low-effort shitposts for the most part, but I still care about having access
to my own data, being able to archive it, use it to train shitpost bots, use it
as a personal database like the [Memex idea](https://en.wikipedia.org/wiki/As_We_May_Think), or whatever. The only selling point
social media companies have is the network effect of everyone else already being
there, and giving away my data because the herd is already in the same place is
pitifully all-too-human, a monkey brain impulse I have no interest in
entertaining.

So yeah, should you join fedi? Maybe, but there&rsquo;s a lot of work to do and it
really depends on how much value you place on your own time and attention. If
you just want to continue to get the dopamine fix being performatively mad
online and get engagement for posting the take of the day or posting an epic
snarky quote tweet of something a bluecheck said or whatever the fuck, then you
will like many others make a fedi account and then leave shortly after to crawl
back to your Silicon Valley bugman drug dealers.


## Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> I saw a post
recently about this which mentioned that the metaphor doesn&rsquo;t even work because
Twitter has a lot of different people on it and isn&rsquo;t really comparable to the
original Eternal September situation of it being a mostly homogeneous group of
bad actors.

<sup><a id="fn.2" href="#fnr.2">2</a></sup> Though maybe for the sake of
user retention it wouldn&rsquo;t be so bad to try to implement some benign form of
this, I don&rsquo;t know.

<sup><a id="fn.3" href="#fnr.3">3</a></sup> And yeah the
political extremists are kind of the same thing to me because they&rsquo;re both
essentially reactionary. I&rsquo;m not even a leftist (post-left anarchist in case you
aren&rsquo;t already aware) but almost no one I&rsquo;ve seen on fedi is a step above a
LARPing &ldquo;anarchist&rdquo; who probably didn&rsquo;t become &ldquo;radicalized&rdquo; until after the
2016 election. There are a few instances that are exceptions to this though.
