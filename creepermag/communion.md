# Communion

"I'm not going to go", I lie to myself, and to them.

Three white bars illuminate the filthy concrete in front of me as I duck through
the grate during the lull in traffic. The bars point up and down, good a compass
as any. I pull a folded up piece of paper out of my pocket with the IRC logs
printed out. "Take right for 128 heartbeats, then press left."

Zero... One... Two... Eight... Sixteen... Thirty-two...

The concrete blocks are loose here. I push left as instructed and with a little
convincing, they move enough for me to squeeze through. "Make sure to close the
door behind you."

This looks like a maintenance tunnel. To the left and right are a mass of cables
running along the wall, pumping electricity to the world above. The ceiling is
just high enough to walk through. The walls have graffiti, and there's some
garbage strewn about. Pieces left behind by bodies being moved through the
tunnels. "256"

I head forward, reach the end of the tunnel, go through the door, head right.
The tunnels get tighter in certain places and my body has to conform to their
geometry, shimmying through gaps in the walls, climbing over rubble, crawling
through tubes with stagnant, shallow water that smells and feels like being
inside a wad of hair that was clogging the drain. I have no clear direction. I
let myself be carried along by the geometry of the tunnels.

These maintenance tunnels branch out all over the underside of the city. There
are no known maps of them, or if there are, they haven't been made public. The
public works crews are sworn to secrecy in this city.

Initially I'd wondered how these instructions found their way to me. You need to
reach a certain rank in order to get them, and even then, it's a challenge to
find your way to the end. Acquiring them required documenting almost as much of
this city as anyone except the public works crew. They know this city so well,
they're practically a part of the infrastructure. Some say that the maintenance
workers can move through the walls of any building without making a sound.
Sometimes you might see a shadow obscure the ceiling light for a moment, or an
eyeball in your sink drain, but no one ever sees them even if their presence is
felt.

The room number is 256, a large maintenance closet. Rows of metal shelves all
throughout, and the faint smell of ammonia, and dust lingering in the air. The
shelves are filled with things you'd expect maintenance crews to have --
wrenches and other tools, that sort of thing. No flashlights, though. And then
there are other devices here. Glass jars with copper antennae, boxes with knobs
and dials inscribed with strange symbols, metal tubes with caps on both ends
that look impossible to open, piles and piles junk that have a secret technical
use.

I know one of them is in here. I feel it. Every time I peer around the corner,
nothing. I have my Maglite out now, with the light on. The entire aisle gets lit
up, but the shelves are stacked with so many tools that it's impossible to light
up more than one at a time, and knocking a shelf over could attract something
worse than a crew member. I could have sworn I once got a glimpse of one a few
years ago, but there was a ringing in my ears and I felt myself falling out of a
tree and then when I could see again, there was nothing. At the end of one of
the aisles, a door. "Go through it."

Open space, concrete floor.

To the right is a short drop into the black. I leap down. Bend the knees on
impact. Nice and clean, like shocks on a buggy.

A few feet away is a faint sound of water drops. One heartbeat, one drop. Behind
me is a building, one of many that the city has been rebuilt atop every time the
city moves. The building is barren and has rows of windows. Barracks, from the
looks of it. Anything interesting in there has long been picked clean. Drip.
Drip. Drip.

There's a dead tree on the other side of the street and a bus stop. The
streetlight still works. The tangled and broken wiring somehow manages to find
its way down here, or perhaps some of the solifuges wired it up. Under the
streetlight is a twitching figure on the bench. Hard to tell what exactly from
the dim lighting.

The streetlights give off enough ambient lighting to distinguish dark forms
moving through the streets, in the alleys and in and out of buildings. The
hearing is sharpened enough to catch the shuffling of feet, whimpering,
sometimes shouts in the distance. There's a Taco Bell here all the heroin
addicts hang out in, and in a hospital some say there's a drug manufacturer that
sells its goods on the surface. The layout doesn't make sense because the
carcasses of buildings from different parts of the continent have accumulated
down here, swallowed up by the city, digesting with the offal of humanity. Some
of them congregate together like hibernating snakes behind the shuttered windows
of gift shops. The sound of mattresses soaked with every bodily fluid imaginable
creaking under the weight of blind schizo cannibals raping drug comatose
runaways isn't something you can hear walking down the street, but the sound
echoes in your mind nonetheless.

The solis have an almost telekinetic organizational structure. You can feel that
they all know you're down here, but it's rare for upsiders to visit. They have
to ascend up to the surface to hunt, crawling out of the sewers and storm drains
and sinks and toilets to bring people down. You never know if you might get
washed away by the next heavy rain or flushed down the sewer. People disappear
all the time, few ever get reported.

Something catches my eye while lost wandering through the streets. A red
curtain, fluttering just slightly out of a window. This has to be the place.
"Enter through the beginning."

A two-story victorian painted with faded and peeling pastels. It sits tilted
slightly to the side, stately but barely holding composure, drunk off the sewage
and groundwater rotting its bones. The top of its tower appears to be missing.
The door is unlocked.

Papers from every conceivable decade litter the floor. Magazines, letters,
receipts, pamphlets on Jesus and quitting drugs. There are more solis shuffling
around, twitching on the floor. One of them brushes past me, almost silently. I
could swear I heard it muttering something, and when I turned around it was
gone. I stood there for a second looking back down the hallway to the living
room and through the doorway, solis breaking the faint light and the lingering
dust in the air from the undercity streets, and one of them walked into my line
of sight. It lets out a rasping, almost silent shriek, lurching towards me. I
shine my Maglite in its face, hoping to blind it. Its eyes have already been
clawed out, but somehow it must have felt me look at it. A gaunt thing with
long, straw-colored hair, one arm hanging by a few tendons. I toss the Maglite
up slightly, spinning the light side towards me. Remember: Flick wrist outward
as you step forward. The handle connects. The skull shatters into many pieces
and they clatter on the floor. My face feels warm and wet. Close door. Wait.
Silence. Hopefully none of the others were alerted. Some kind of crunching
sound. Something being dragged.

The staircase is destroyed. I have to climb up broken pieces of wood on all
fours, moving as slowly as possible, gently putting a hand or foot down, testing
the stability of the ruined steps, watching out for nails and needles and such.

Finally at the top. There's a bedroom to the left, boarded shut. There's a black
door at the end of the hallway. Paintings and photos cover the walls. Most are
indistinguishable and look as if they'd crumble to dust if you tried to pick
them up. The black door is unlocked.

The walls and floor are mostly barren. Iron rods, stockades, crosses, filthy
looking whips and rusted chains, scalpels, benches. Places like this are kept a
secret to everyone who isn't involved in extreme fetish communities. Maybe the
secrecy and the filth gets them off. There's no dust in the air here. It feels
slightly warm, and smells like sweat.

On the wall against the tower is a vent just barely big enough to fit an average
sized person. After fiddling with the screws for a moment, I'm able to squeeze
through. After reaching the other side into the tower, I'm dumped into a pile of
ash. "Down."

The air is thick with ash and dust and my lungs are desperately trying to get
fresh air into them. After managing to get almost a full breath, I dive in,
burying myself alive. Rings, necklaces, coins, teeth all move past me as I dig.
Eventually the ash turns to peat, filled with human bones. The air here is thick
with death and plague. It moves much easier now; I'm practically swimming
through a mass grave. It's impossible to say how deep I've gone by now, yet
somehow I haven't run out of breath.

Suddenly I fall out onto a stone floor. The ceiling above is a pile of dirt that
somehow doesn't fall down. An arched doorway is ahead of me. "Welcome back."

I step through to rows of pews. Centuries old, at least. The ceiling is domed,
the walls adorned with stained glass windows that somehow are wholly intact and
almost seem to glow. Bas-reliefs of saints and scenes from the Bible are set
into pillars, yet the bas-reliefs and the stained glass windows all depict
scenes I've never heard of from the Bible. A snake with the head of a lion
crawls out from between the legs of a woman, or perhaps she's inserting the
snake into her. Above the altar, the centerpiece of the room, a constellation of
spheres paired together, branching outward in the shape of an egg with a spider
on top of it. The egg is engulfed in mauve flames, and a pink yolk seeps out
from cracks in it. Figures beneath the egg lay prostrate before it, in fits of
moribund ectasy.

I approach the altar, where a chalice awaits depicting uncannily beautiful women
who seem to beckon my lips to meet the rim. It is filled with the same pink yolk
in the stained glass scene. Even while perfectly still, it doesn't stop moving,
as though a wind were blowing whitecaps across the sea. One of the caps leaps up
slightly, I hear a faint tone in my ear that I've never heard before. I drink
from the chalice, which tastes like ambrosia. The mauve flames engulf me from
the base of my spine to the top of my head and a forked symbol cuts me in half
into two new beings for just a moment before the pink yolk shoots out between
the vivisection wounds and pulls us back together into one. I feel a ripple
through my body. The wall beneath the stained glass opens into another arched
doorway, a black, vertical body of liquid.

"Go forward", it whispers.

