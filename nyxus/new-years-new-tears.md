---
title: "New Years, New Tears"
date: 2018-01-01T16:06:30-07:00
type: "post"
categories: ["aphotheosis"]
tags: []
draft: false
---
In 1916, the Marxist philosopher Antonio Gramsci wrote in his article "I Hate New Years Day":

> Every morning, when I wake again under the pall of the sky, I feel that for me it is New Year’s day.
>
> That’s why I hate these New Year’s that fall like fixed maturities, which turn life and human spirit into a commercial concern with its neat final balance, its outstanding amounts, its budget for the new management.[^1]

Before the implicit Judeo-Christian culture that continues to dominate much of the West defeated the Romans many centuries ago, there were pagan tribes in Europe that we can trace many of our holidays in the US back to. And these pagan tribes, famously, were assimilated into Roman culture after being conquered by having parts of their culture assimilated into the Roman Borg culture conglomerate. Thus we have the origins of Christmas, not the day Jesus Christ was actually born on, but a convenient hyperstition to get the pagans to accept Jesus into their lives by associating him with the Winter Solstice.

Things have not changed, only become more refined. Holidays are no longer Christianity flexing its muscles as the dominant conquering religion. Superficially, they have long appeared that way, but only as a front to assimilate us into relations of capital. Christmas has evolved from the pagan solstice holiday to the Christian birth-of-the-savior holiday to a great, violent sacrifice for capital.

The notion of a Heaven and a Hell where people will be judged and sent to when they die is no longer a sufficient narrative for making people be good. The longer our lives become, the less death is a part of it, the less impactful it feels to say that we're going to one day die and be judged. We may be judged then but we have a lifetime to prepare to be judged and a lifetime to have fun before repenting. This is only intensified when the idea of living into old age and being able to retire increasingly becomes a thing of the past, after the post-war honeymoon stage of American capitalism has worn off for more than the most maligned and marginalized groups.

If Santa Claus didn't exist, he would have to be invented in the future and sent back in time. Christianity's linear progression of birth-death-judgment is no longer sufficient. Time has become fragmented for us, and the "continuity of life and spirit" that Gramsci talks about in his New Years piece has likewise been ruptured. Our phenomenal experiences have no clear, pure locus in the world; they are in the wires, abstracted, everywhere and nowhere at once. And likewise, surveillance is not located merely in police and judges; it is everywhere and nowhere.

There is perhaps no simpler and more baldfaced metaphor for Oedipus than Santa Claus. He sees you when you're sleeping, he knows when you're awake. He is everywhere and nowhere at the same time, and he's coming to town.

Even this, however, is no longer a truly sufficient metaphor for capitalism to smuggle itself into our culture. Christmas happens once a year, and everything leads up to it. We learn as we become well-behaved adult players in the Oedipal drama that Christmas isn't a time when we are rewarded for getting free stuff; rather, Christmas is a market exchange where we, ideally, will break even. A great sacrifice of capital (and in the case of Black Friday, often some lives) to mark off the year with equilibrium. The waste and excess that capitalism requires is forgotten.

This is why the subject of this post is on New Years, because while Christmas is the performative and outmoded spectacle of A-Time, New Years is what underlies this spectacle. The notion that time can be marked off neatly by cycles of waste, equilibrium, renewal -- a mirror of the cycle of capital. That with each new year, things are any different.

Everywhere on social media, even among so-called radicals (even if they might claim it's ironic), A-Time shuffles through the wires, yet New Years happens for us as many times as it happens for the world, in real time. New Years for me felt like nothing, as I had already experienced a hyperreal midnight several times that day. There were many New Years that day for me; already I am in the year 2020. And it is always midnight somewhere for us when everywhere we go, we have a device the plugs our phenomenal experiences into a matrix of shared social spectacles. How many years we age in one year, how much further we spiral into being past our life expectancy. Just as it was for our neolithic ancestors, by 30 death is already a part of our everyday lives.

The only thing that changes with each year are how many more ruptures in time have been torn.

[^1]: https://www.viewpointmag.com/2015/01/01/i-hate-new-years-day/
