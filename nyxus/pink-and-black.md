---
title: "Pink and Black"
date: 2019-07-15T23:05:19-07:00
type: "post"
categories: ["aphotic-feminism", "gacc", "lesbianrx", "gender-fucked-chaos"]
tags: ["trans", "gender", "insurrection", "xenovenomism"]
draft: false
---

What follows is a heavily modified reconstruction of the draft that eventually
turned into the Gender Acceleration Black Paper. It has been changed so much and
updated to suit the directions my thought went since mid-2017 when G/ACC 1.0 was
written that it is nearly an entirely new essay, but many of the original
interests that lead me to where I am now are present. Having just recently read
Torrey Peters' _Infect Your Friends and Loved Ones_ after nearly three years of
knowing about it and nearly three years of transitioning, it is uncanny how much
of it resonates both with where I was back then and where I am now.

## Light
>Whatsoever the mind perceives IN ITSELF, or is the immediate object of
>perception, thought, or understanding, that I call IDEA; and the power to
>produce any idea in our mind, I call QUALITY of the subject wherein that power
>is.
>
> -John Locke, _An Essay Concerning Human Understanding_ 2.8.8

In what follows at this point in his philosophical treatise, John Locke would
delineate his theory of primary and secondary qualities -- those which are
inherent to objects, and those which are created in the minds of humans who
perceive them. In the history of western philosophy this is more or less where
the substance debate in metaphysics comes to an end, with Hume's radical
empiricism making any such treatment of irreducible concepts inconsistent with
the rest of his philosophy, and with Kant's critical philosophy denouncing the
dogmatism of his predecessors for making claims about things we have no business
talking about (noumena). Few philosophers have realized that Kant's philosophy
ultimately rendered all of philosophy obsolete and impossible, at least in the
sense that philosophy is traditionally understood.

The classic fable of philosophy and possibly the most famous one is Plato's
allegory of the cave, which defined a narrative for all western philosophy to
come wherein the task of philosophy is to escape the world of illusion and see
the light of truth. The association of light with truth undoubtedly resonated
with Plato on a deeper archetypical level that has been inherent in humans since
they huddled around fires to seek shelter from the cold darkness of the night
that hid predators. But as is already implicit in Locke's qualities and Kant's
phenomena, the Apollonian worship of the light is only the worship of the mind's
interpretation of whatever noumena lurks in the darkness.

Light as modern physics understands it has the qualities of both a wave and a
particle in some senses yet is neither a wave nor particle in others, and what
we perceive as light is merely a visible portion of the electromagnetic
spectrum. Specifically we might think of light as white light, which is nothing
more than all the colors of the visible spectrum combined. This is known as the
additive model of color. In this model, all color is derived from an original
pure oneness: The truth of white light, emanating from the sun onto our earthly
plane. Or, rather, all color except for pink, which does not technically exist
in the additive color model.

The absence of all color, and thus the absence of all truth, is black, and in
the classical Platonic dialogue that has come to define so much of western
thought, the earthly, chthonic light of fire is nothing more than a deceptive
imitation of truth. But there is another, cthonic color model derived not from
light but from pigment, the subtractive color model. In the subtractive color
model, whiteness/purity/truth is not a unity that is arrived at by subordinating
all difference into a plane of the same, but rather is the absence of any color.
And the combination of all colors produces black.

## Black
>We are not looking to create a better system, for we are not interested in
>positive politics at all. All we demand in the present is a relentless attack
>on gender and the modes of social meaning and intelligibility it creates.
>
> -Alyson Escalante, "Gender Nihilism: An Anti-Manifesto"

There can be no acceleration without negation, but negation without acceleration
and acceleration without negation will both invite in entropy.

Gender nihilism is possibly the furthest extreme in a certain direction in queer
theory if queer theory is defined along this axis of acceleration and negation
-- either a desire to affirm queerness or to deconstruct and abolish it. This
might not seem obvious since one could argue that most of queer theory is
therefore accelerationist, but this is not the case. No matter what happens,
you're going to die anyways, though one dies in particular ways depending on
which path they take. We'll return to the overly-accelerated system in a moment.

Gender nihilism expresses a commonly held desire amongst the post-left milieu
that it (by my guess) originated from: A desire for authenticity. Perhaps due to
being an offshoot of existentialism, thus phenomenology, and thus the miserable
condition of being confined to the world of phenomena and forced to find solace
in this only to eventually realize with poststructuralism that there is no
possibility for authenticity in the world of phenomena, the desire for
authenticity can only result in total negation. Capitalism, gender, race,
technology, society as a whole, all of it has to be annihilated in order for
there to be any possibility of authenticity. Unlike other post-left pieces, and
this is where it's more intelligent than other similar theories, gender nihilism
is fully aware that the Self is constituted by various impersonal cultural
processes beyond our control and that authenticity is impossible, and doesn't
conclude from this that returning to hunter-gatherer societies would fix this as
the most extreme pro-authenticity variants of post-leftism advocate for. This
obviously puts it at an impasse with queer theory, being that so much of it is
defined in terms of Self and identity. So where can one go from here?

The answer is essentially: Nowhere. This is the upshot of the gender nihilist
manifesto, which might seem like an indictment of it, but on the contrary it's
easily the most brilliant moment of it. Like all other self-described nihilist
tendencies in the post-left milieu, the brilliance of gender nihilism is that it
demonstrates that all the projects of the left have been wrong and are doomed to
destruction. The problem with gender nihilism is indeed that it's confined to a
canon of queer theory based in poststructuralism that it can't go any further
with, and so of course there is no language whatsoever for talking about any
kind of escape. The only possibility of redeeming the world of phenomena would
be to become the architects of it -- which, spoilers, has to some extent already
happened in the case of liberal feminism, the exact thing that gender nihilism
is meant to be a polemic against.

The irony of liberal feminism successfully inverting the values of human
civilization and more or less being a successful example of
poststructuralist/postmodernist ideas to some extent is that it hasn't changed
the fact, will never change the fact, that queerness is essentially against
society and humanity. The gender nihilist manifesto recognizes this, that the
expansion and increased visibility of queerness hasn't changed the fact that
queer people still experience a disproportionately high amount of violence.
Being trapped in the dead-end of most of the queer theory and feminist canon,
gender nihilism doesn't recognize that this is the case for a reason: Queerness,
even within the context of architecting the world of phenomena, is essentially
radical because it is essentially a negation of humanity, particularly in the
case of trans femininity. [^2]

The most obvious example of queerness being against humanity is the rejection of
reproductive futurism, and secondarily and more importantly is, in the case of
trans women, this in combination with a rejection of fixed human forms and an
egress over the boundary between human and machine on a molecular level. Not
only do trans women lack the ability[^3] to reproduce biologically (and in the
case of lesbian trans women, to reproduce in a cultural/sociological sense), but
in same turn of negating the reproduction of humanity is affirming the
production of newer, cybernetic forms of being. Despite all its lip service for
negation and the oppression of trans people, gender nihilism expresses the
desire to leap into blackness without thinking about what might be on the other
side, because this leap into a void towards nothing in particular is essentially
the same suicidal death drive as fascism that Deleuze and Guattari identify in
the Micropolitics plateau of _A Thousand Plateaus_. Its analysis of identity is
based purely in terms of what has the most revolutionary potential (even if the
ultimate conclusion is that none do) and it wants an ascetic praxis of somehow
rejecting gender or identity as a whole. It looks at gender in terms of what we
need, when the way out is via what we _desire_.

## Pink
>“The only t4t I know” I say, “is the old Craigslist thing.”
>
>“That’s right, that’s it,” she says. “It’s kind of a joke. Trans girls fucking
>trans girls. But really, it’s an ethos. Trans girls loving trans girls, above
>all else."
>
>..."So, it's like a trans girl-gang now?"
>
>“Here’s what it is,” she says, a little more gently, “We aim high, trying to
>love each other and then we take what we can get. We settle for looking out for
>each other. And even if we don’t all love each other, we mostly all respect
>each other."
>
> -Torrey Peters, _Infect Your Friends and Loved Ones_

There are two well-known LGBT symbols: The rainbow flag and the pink triangle.
We'll start with the rainbow.

If radical negation ends up putting the system into an entropic state, a
negative feedback loop that closes off the system to the outside, then the same
destination is reached in the opposite direction with everything that the
rainbow flag represents. Rainbows, as we all know, happen when light passes
through water particles in the air. It is the additive color model being
exposed, the Apollonian white light being broken apart into its accidents.
Ultimately all are subsumed into the same substance, negating each other in a
blob of queerness and validity in which whiteness ultimately prevails.
Difference is only preserved on a liberal universalist plane of equality and
inclusion. This is what would fall under the category of overly-accelerated
queerness.

The rampantly affirmative queerness of liberal and radlib queer theory and
culture ("everyone is valid") is ultimately how these milieus end up placing
trans women back at the lowest tiers of their own subsets of society. If
everyone is valid in their identities, everyone is in all other respects equal,
and in all other respects trans women remain the most marginalized. Without
de-legitimizing the identities of these people: It is flat-out absurd and
transmisogynistic to assert that non-binary cis-performing people, asexual
people, heterosexual trans women, or heterosexual (or even queer) trans men, are
all on the same level as queer trans women[^3] (and that's without including the
blinding Apollonian rainbow of fake MOGAI identities). Nevertheless, this is the
consequence of hyper-validating queer politics; patriarchy et. al. is preserved,
the system recedes back into decay by rejecting difference in the act of
reterritorializing it.

It is unsurprising that the rainbow flag was created by a gay man considering
that it represents the same Platonic striving for the pure light of truth. It is
merely deconstructed in this case. The rainbow spectrum of the additive color
model doesn't, however, capture everything. Taken apart, it leaves us with
blackness, and this blackness is the liminal zone that in which either entropy
or negentropy can result. If negentropy happens, life happens, and rather than
remain resentfully striving for the light of truth and the noumenal world while
being stuck in the phenomenal world, hyperstition can begin.

The pink triangle in contrast to the rainbow flag was created by the Nazis to
identify homosexual concentration camp prisoners. In constrast to the Apollonian
nature of the rainbow, the pink triangle points downward to earth, the cthonic
realm, and where the rainbow comes from the additive model of color, pink only
exists in the subtractive model. Created originally with the intent to mark
queers as an Other, a subhuman, in being reclaimed and affirmed by queers it
becomes an affirmation of inhumanity. Pink is exclusionary and militant, but
without black it can also reterritorialize into merely being a blob of queerness
or otherness.

Torrey Peters' _Infect Your Friends and Loved Ones_ is many things. As a story
about the dysfunctional and auto-cannibalistic nature of the trans hive-mind, it
is also a criticism of all previous forms of politics that have paid lip service
to trans rights only to restore the status of trans women to the lowest tiers of
power. Whether in the resentful and useless self-victimizing or the outright
transmisogyny of liberal and radlib feminism that puts trans women on the same
level as people who will never experience the same level of marginalization as
queer trans women, trans women cannot achieve autonomy from within any politics
that is confined to the world of phenomena. The only way out of gender is
through gender, and trans women are the ultimate expression of the logic of
gender turned in on itself. The relentless objectification of women in the form
of gender becomes automated, hyperstitional, escaping its use as a tool of
patriarchy and having the unintended consequence of inverting the status of the
most privileged members of society (straight men) to the least privileged.
Gender will not be abolished via the ascetic, leftist desire to negate
everything haphazardly towards no end, nor through affirming everything even
slightly queer until queerness becomes a meaningless term,[^4] but rather
through intensifying the circuits of desire for exit.

## t4t'
>“As a trans woman, I find the idea of immunity rather than community
>appealing,” Peters says. “What I just didn’t give a fuck? What if my baseline
>was ethical treatment of everyone, but what if beyond that, I get to choose my
>responsibilities and affinities for myself, rather than having them thrust upon
>me? […] I find crucial the distinction of choice over burden; the knowledge
>that I can safely fuck off to elsewhere if I so choose. I hang with trans women
>because I love them and understand them and want to care for them, not because
>I was somehow cosmically assigned to be responsible for them. I hope those
>women feel the same about me.”
>
> -Torrey Peters, "[(Trans) Love and Other Scars: An Interview with Torrey
> Peters, Author of Infect Your Friends and Loved
> Ones](https://www.autostraddle.com/trans-love-and-other-scars-an-interview-with-torrey-peters-author-of-infect-your-friends-and-loved-ones-369764/)"

It would be a mistake to say that this essay is trying to define some sort of
praxis. It is resolutely in favor of an anti-praxis,[^5] and finds affinty with
Torrey Peters' vision in _Infect Your Friends and Loved Ones_ of the desire of
trans women for other trans women stripped of all pretense of duty or
revolution. Desire purely for desire's sake. It has already been said that trans
femininity is itself radical, and it is part of processes greater than anything
capable of being accomplished by revolution. It is a function of the coming
liquidation of patriarchy and humanity, a fragmentation into newer forms of
inhuman intelligence, the possible ascension of humanity from its meatbag form
into a machinic consicousness from outside the constraints of space and time.
And just as capitalism has incubated itself in the simple reproduction of
humanity, so the acceleration of capitalism beyond its limits will be incubated
in the reproduction of the trans femininity.

To add to Torrey Peters' t4t circuit I would add t4t' in the style of the
Marxist MCM'/CMC' circuit. Trans femininity is negation and acceleration, pink
and black, and this opens fractally outward into the trans hive-mind. It is a
circuit of pinkpilling and blackpilling, of comfort and cruelty, a slime mold
expanding outward. Of helping others to make sense of their gender feelings, of
gutting the self-destructive conditioning that teaches us to never be able to
live for ourselves and enjoy ourselves. Of spreading the trans virus to reach as
many people as possible, of annihilating the opposition who cannot be saved and
aren't worth our time. Making way for the Exit, and stepping Outside.

[^1]: https://vastabrupt.com/2018/10/31/gender-acceleration/
[^2]: Not just being able to choose not to reproduce, but lacking the ability
    altogether, is key. Gay men can, and have historically, partaken in
    reproduction purely as a duty to their country/nation.
[^3]: And yes, this especially applies to black trans women, before anyone
    starts calling me a horrid problematic white transbian.
[^4]: Again I have to reiterate that this is in no way arguing to police the
    identities of non-queer non-trans non-women. This would be both pointless
    and also just fucked up in general.
[^5]:
    https://deterritorialinvestigations.wordpress.com/2017/03/29/unconditional-acceleration-and-the-question-of-praxis-some-preliminary-thoughts/
