---
title: "Excerpt from 'The Dark Deleuze Rises'"
date: 2017-07-13T14:45:21-07:00
type: "post"
categories: ["shitposts"]
tags: ["rhetttwitter", "cavetwitter", "memes", "baneposting"]
hidden: true
draft: false
---
_Night City: 2020. A thin, lumpy-faced man strolls through the rain-soaked streets, downs a handful of Dex. He is clearly disheveled as he makes his way to the bombed-out Villa Straylight. His trenchcoat is a black flag whipping in the wind as he heads to the exit. Waiting outside for him are Sadie Plant, Mencius Moldbug, and three masked men._  

Nick: Dr. Plant, I’m Ccru.  

Sadie nods.  
_Nick spots the three hooded men with Sadie._  
Nick: You don’t get to bring friends.  

Sadie: They are not my friends.

Moldbug: Don’t worry, no bitcoins for them.

Nick: Why would I want them?

Moldbug: They were trying to unleash acceleration. They work for the PhD student. The U/ACC man.

Nick: Vince?

Moldbug nods.

Nick: Get ‘em in cyberspace – I’ll call it in.  
_The six enter the building and proceed to a room lined with decks. They jack in and enter the grid._
_Nick grabs one of the hooded men._  
Nick: What are you doing in the middle of my hyperstition?

The hooded man says nothing. Nick loads up the ice.

Nick: The exit plan I’ve drafted lists me and my colleagues, but only one of you.  
_Nick hangs the hooded man over the ice. He yells,_  
Nick: First one to talk gets to stay in my grid!

Nick: Who funded you to cite Dr. Plant!?  
_The hooded man says nothing still. Nick throws him into the ice and derezzes him._  
Nick: He didn’t hack so good! Who wants to try next!?  
_The killbots grab the second hooded man._  
Nick: Tell me about Vince! Why does he live in the cave!?

The second man says nothing.

Nick: Lot of loyalty for a twitter follower!

Third prisoner: Or maybe he’s wondering why someone would accelerate without annihilating human agency?  
_Nick turns to the third prisoner and turns off the ice._  
Nick: At least you can talk. Who are you?

Third prisoner: We are nothing. We are the subtweets beneath your takes. And no one cared who I was until I let go…  
_Nick approaches the third prisoner and pulls of the mask, revealing a bespectacled young man. The light glints off of them, mirrored and bug-like. This is Vince._  
Vince: What we do doesn’t matter. What matters is tomorrow’s plan.

Nick: If I let go, will I die?

Vince: It would be extremely painful.

Nick: You’re a speedy guy.

Vince: For you.

Nick: Was getting caught part of U/ACC antipraxis?

Vince: Of course! Dr. Plant refused our offer in favor of yours. We had to know what she told you about us.

Sadie: Nothing! I said nothing!

Nick: Why not just ask her?

Vince: She would not have tweeted back.

Nick: You have methods.

Vince: Her, I need healthy. You present no such problem.  
_The grid becomes destablized. A shadow looms over it._  
Nick: Well congratulations, you got yourselves caught. So what’s the next part of your antipraxis?  

Vince: Accelerating the process…  
_The grid is visibly fraying as an AI and several Trog Moderns jack in._  
Vince: …unconditionally!  
_The AI makes quick work of the killbots as Vince, in the confusion, takes this moment to grapple Nick. The ice is turned back on. Mencius tries to jack out, but in meatspace, the Trog Moderns have already taken over the Villa. Before he can punch deck, he’s been flat-lined._  
_Vince overpowers Nick and throws him into the ice. Cyberspace is about to fall apart at the seams. U/ACC’s work is done here, but as Vince and the others are about to leave, he stops one of them._  
Vince: They expect one of us in the wreckage, 🅱rother

Trog: Have we started the meltdown?

Vince: Yes, the process accelerates!  
_Vince grabs Dr. Plant as he prepares to jack out._  
Vince: Calm down, Sadie. Now’s not the time for fear. That time has passed.  
_Vince and Dr. Plant exit as the AI begins to assimilate cyberspace into itself._
