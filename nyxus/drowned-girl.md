+++
title = "Drowned Girl"
date = 2020-04-24T01:09:00-07:00
categories = ["theoryfic"]
draft = false
type = "post"
+++

JANUARY 27TH 2018 AT 12:38AM: The waves inhale and exhale particularly strongly,
the moon above lighting the scene. A girl stands on the shore, just out of reach
of the waves. The sands churn with each crashing breath and the dead are spit up
onto the molecular planes. The miasma of rotting fish and seaweed comingles with
the salt in the air, the steady white noise, the dimly visible silver sheen of
the sharp striations in the water which smooth out the moment they appear. The
waves creep up further, swallowing up more of the shore, the icy cold Pacific
waters reaching her numb, bare feet. She steps out, the water rising up her
legs, enveloping and penetrating between her thighs, shocking the nervous
system. She stops for a moment, forces herself back into motion, unable to feel
anything from the waist down. The water is up to her chest, gripping her neck,
then pulling her in.

The caps rise and fall, battering her, crashing down by the weight of their
dying intensities. She stays afloat uncertainly, suspended over a dark well of
infinite possibilities. The winds sadistically blast her soaked face which has
already turned a translucent white before she lets go and descends.

She opens her eyes to see an inverted horizon dimly lit by the moon. Her body
has become entirely numb; she no longer has any sensation of a body at all. She
may as well no longer have one, and as the water begins to rush in, oxygen being
cut off from her brain, lungs bursting open and leaking saltwater into her chest
cavity, she no longer has any thoughts. In the flatline moment of absolute
terror and physical agony, time is suspended over the unknown depths of the
present. She begins to sink.

She has never been anywhere other than here, drowned and descending through the
darkness. Her body is no longer hers; it has become autonomized, belonging to
the flows of dead fish. She diagrams a former existence that had never actually
existed, a waking dream that slips into a dreamless sleep. A waking dream of
being human, a dreamless sleep of being a woman, a molar identity with
subjective instrumental control over a body which becomes attuned to the oceanic
flows of death, catastrophe, infinite succession and regression, and immemorial
deep time. In her former human existence, she has no utility, exists under the
tyranny of subjecthood, relentlessly reterritorializing the immanent strata of
the earth and making it an object that exists for the subject. The human being
takes objects as being there for them, practically coming into existence the
moment they are needed before receding into the tangled synapses of memory. They
surface, resurface, themselves hidden beneath the surface of the earth.

Her former existence as a human being, as a subject which claims for itself a
body, was never womanhood. On the surface, beyond the shores where intensity
reaches its highest pitch before flatlining, she existed as a deceptive image of
humanity harboring within it a demon. This image of humanity, this false memory
of a former life on the surface, this is the waking dream that held within it
the exit, the summoning sigil inscribed on the shores that is taken out to sea
and thrown about in the undercurrents of the lesser depths. It slips from a
memory of humanity deeper into the depths.

The body, the drowned girl, descends deeper. Her ankles are tickled by the
passing fingers of forest kelp, fish swim past, inspecting and perhaps biting
off a chunk of flesh if they're able to. The body has become waterlogged, the
pigment which stored the memory of a dreamed sun fading out of her skin and
mixing with the salt and miscellaneous filth. The current drags her deeper
still.

She has never existed as anything other than a body suspended over an abyss, in
a transitory state of drowning, as a body without the capacity to contain a
subject. She is stillborn in the oceanic womb, never having the capacity for
utility to either record memories and represent, never able to be represented
and recorded, neither a site of production nor a site of exploitation. The
infinite second between life and death, the incommunicable knowledge of a
decapitation victim's 20 seconds as a head without a body. She has no past and
no future, and in this second before absolute annihilation she will find
infinite possibilities laid out before her which deny even the solace of total
dissolution. "Annihilation is much harder than you think."

The drowned girl's body is no longer within reach of the moonlight. As it slips
deeper down, it departs from the earth, is part of the perpetual night sky
below. The body's flesh is pruned, eyes staring into the nothing as hagfish
burrow into her chest cavity and hollow her out. They force their way through
the spaces between bones, into the stomach, stretching out veins. Sharks swim by
and bite off chunks of flesh.

Because they only view things as they exist for-them, human beings have never
supposed that water could exist as other than in relation to their own health.
Water carries with it both disease and cleansing, but only through drowning can
one make the transition and see water for what it really is. Saltwater in
particular, which is simultaneously purified through the salt yet also hazardous
to human health, reveals the unconditional molecular nature of water. It erodes
the shores and drowns organisms from the surface. Its depths deny the existence
of any living things with the capacity for memory. Its time is colder and more
inhuman.

Her bones settle in the middle of a vast black desert, awaiting the death of the
surface, the death of memory.
