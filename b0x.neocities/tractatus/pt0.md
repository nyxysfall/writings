# Foreword: Resurrection Ritual
1. The Self appears to itself at its funeral
2. To be dead is to be broken glass which does not comply with glue
3. The Self is indifferent to itself

4. Indifference is the appearance of Nothing in blue
5. Nothing in blue instantiates blueness which signifies substance
6. Nothing is the Self by itself: The Ego

7. The Ego is the phantom highway on the way to Self
8. The Self reaches its dead Self along Ego
9. The Self becomes Nothing, thus Ego, thus its Self
