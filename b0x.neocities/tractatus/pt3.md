# 3.1 Material: Self

1. Nothing as perception breaks what is already broken 
2. Being already broken is broken further in perception
∴ Being becomes Being in an original wreck

4. The glass flying from perception is in-itself invisible
5. Glass is made visible by its shadows
∴ Within the becoming of Being is Nothing

7. Being is perceived Body, Mind is perceiving Nothing
8. Body becoming Body contains Mind
∴ Body is Self, immediate Mind

# 3.2 Material: Ego

1. Glass becomes Glass when its shadows reveal it
2. Shadows only exist as the Nothing of light
∴ The Self is fundamentally a prism

3. Light itself is neither a wave nor a particle
4. Shadow becomes shadow against a paradox
∴  The Mind in Body is formless Body 

7. Glass without shadow isn’t real
8. Shadow is the negative of the becoming light
∴ Ego is true Self, but also mediated by Self

# 3.3 Material: Causa Suicide

1. Glass in-itself is invisible and appears through its shadows
2. Body as Self is immediate Mind, not reflected mind
∴ Self is merely the appearance of the Ego

4. Ego alone is blind and invisible but wishes for recognition
5. Self is Ego augmenting its vision and its corporeality
∴ The Self is an apparatus of the Ego, not the Ego itself

7. The Ego is the creative Nothing that continually appears as Self
8. Every Self is insufficient Ego killed by the next Self in line
∴ The becoming of Being and Nothing is Being to Nothing: Causa Suicide
