# 1.1 Construction: Physis

1. There is no Being in general without satisfied Being
2. Satisfied Being is Being in-itself and for-itself
∴ Being-in-itself-for-itself is what is most actual

4. A shard of glass contains nothing more than glass
5. Glass is unyielding and irreducible to anything else
∴ Actuality is shards of glass spun into silver gossamer

7. Physis is the LAW and thus irreducible
8. Only actuality is a body irreducible – a unity
∴ Physis is satisfied Being, a knot of spun actuality

# 1.2 Construction: Nomos

1. There is no Body without Being
2. Being on its own is Physis
∴ Body is an appearance of Being

4. Being as appearance is unsatisfied Being
5. Body is glass shards glued together
∴ Body is anxious Being

7. Nomos is laws and is thus contingent
8. Being is only contingent as appearance
∴ Nomos is unsatisfied Being, a glass urchin figurine

# 1.3 Construction: Anarchy

1. Physis is a knot of actuality, irreducible
2. To be irreducible is to deny the epistemic Look
∴ Physis is satisfied Being in its indifference

4. To be indifferent is to want for nothing
5. Nomos is a glass urchin figure, contingent
∴ Nomos wants for the indifference of Physis

7. Physis as Being in blue denies an Other
8. Nomos as the prism of Being has no Other
∴ The only law is chaos
