# 2.1 Deconstruction: Destruction

1. Being as a self-contained meaning is complete
2. To want for something is to be an incomplete thing
∴ Nothing as not-a-thing that wants  no-thing is pure indifference

4. Pure glass is the Nothing in Being, immediate indifference
5. Shadow is the Nothing in light, pure indifference
∴ Shadow makes glass visible and thus vulnerable

7. Shadow appears in appearing glass
8. Shadow as a Nothing wants for no-thing
∴ Glass is destroyed by its shadow

# 2.2 Deconstruction: Insurrection

1. Being is immediate indifference – self-contained meaning
2. Being nevertheless appears to an other in its shadows
∴ The shadows torn from glass are the appearing of Being

4. For glass to appear it must also be perceived by an Other
5. Glass as Being nonetheless still wants for no-thing
∴ Glass is perceived by the Other that for it is Nothing

7. The Nothing that perceives glass is in-itself a thing
8. The Nothing as pure indifference defies all Others absolutely
∴ All things are Mind, Nothing in constant insurrection


# 2.3 Deconstruction: Apocalypse

1. Pure glass as Being is already-broken glass unities
2. The appearance to Mind of shadow destroys a glass unity
∴ Perception breaks further what is already broken

4. Mind is the perceiving Other to glass
5. Glass is the self-contained Same perceived
∴ Breaking-broken glass is Body perceived by Mind

6. Mind as perceiving Nothing is nonetheless a thing to itself
7. Body as perceived Being is nonetheless no-thing to its Other
∴ Everything is constantly ending its phenomenal existence
