# Preface: Proof against life: 1.1: Being.1

1. Being requires silver strands of actuality
2. Body is a cobweb knot of actuality
∴ Body is a unity of unities

4. Unity is the broken glass shards at the feet of rampaging contingency
5. Every unity of glass is a contingent unity 
∴ Body is a contingent multitude

7. Body is a multitude of unities
8. Unity as such is indifferent to other unities
∴ Being is the blue indifference shining through Body


# Preface: Proof against life: 1.2: Nothing.2

1. Nothing wants for no-thing
2. Indifference is the weeping sage with dry tears
∴ Indifference is the appearance of Nothing

4. Appearance is a shadow torn from a unity
5. Perception is the act of cleaving a unity
∴ Mind is what perceives appearance

7. Perception is Mind walking through a field of fog
8. Body is the grass shining with glass drops of dew
∴ Being is no-thing

# Preface: Proof against life: 1.3: Becoming.3

1. Being is thrown from the wreckage of perception
2. Nothing winks in the flying glass shards 
∴ Mind stands on Body

4. Body collides with glass multitudes
5. Mind is what steals the unities’ shadows
∴ Mind without Body has no eyes

7. Being is the shadow of Nothing
8. Mind is Body perceiving itself
∴ Body becomes Mind, Being becomes Nothing
